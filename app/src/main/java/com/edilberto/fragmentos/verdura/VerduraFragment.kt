package com.edilberto.fragmentos.verdura

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.edilberto.fragmentos.R
import com.edilberto.fragmentos.adapter.VerduraAdapter
import com.edilberto.fragmentos.model.VerduraEntity
import kotlinx.android.synthetic.main.fragment_verdura.*


class VerduraFragment : Fragment() {


    private var listener : onVerduraListener? = null
    var verduras = mutableListOf<VerduraEntity>()



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_verdura, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is onVerduraListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnContactListener")
        }


    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Data()
        context?.let {
        lstVerdura.adapter=VerduraAdapter(it,verduras)

        }

        lstVerdura.setOnItemClickListener { _, _, i, l ->

            listener?.let {
                it.selectedItem(verduras[i])
            }


        }


        listener?.let {
            it.renderFirst(first())
        }




    }
    private fun first(): VerduraEntity? {
        verduras?.let {
            return it[0]
        }
        return null
    }

    override fun onDetach() {
        super.onDetach()
        listener=null
    }



    private fun Data() {
2
      val  verd1=VerduraEntity(1,"Cebolla","S/. 25.00","Allium cepa, comúnmente conocida como cebolla, " +
              "es una planta herbácea bienal perteneciente a la familia de las amarilidáceas." +
              " Es la especie más cultivada del género Allium",R.drawable.cebolla)
        val  verd2=VerduraEntity(2,"Ajo","S/. 10.00","Allium sativum, el ajo, " +
                "es una especie tradicionalmente clasificada dentro de la familia de las liliáceas pero que " +
                "actualmente se ubica en la de las amarilidáceas" +
                " aunque este extremo es muy discutido." +
                " Es la especie más cultivada del género Allium",R.drawable.ajo)
        val  verd3=VerduraEntity(3,"Pepino","S/. 5.00","Cucumis sativus, conocido popularmente como pepino, es una planta anual de la familia de las cucurbitáceas" +
                " Es la especie más cultivada del género Allium",R.drawable.pepino)
        val  verd4=VerduraEntity(4,"Tomate","S/. 35.00","Solanum lycopersicum, cuyo fruto es el tomate, conocida comúnmente como tomatera, es una especie de planta herbácea " +
                "del género Solanum de la familia Solanaceae; es nativa de América Central y del norte y noroeste de Sudamérica; " +
                "su uso como comida se habría originado en Sudamérica hace 2600 años",R.drawable.tomate)
        val  verd5=VerduraEntity(5,"Zanahoria","S/. 25.00","Daucus carota subespecie sativus, " +
                "llamada popularmente zanahoria, es la forma domesticada de la zanahoria silvestre, especie de la familia de las " +
                "[umbelíferas, también denominadas apiáceas, y considerada la más importante y de mayor consumo dentro " +
                "de esta familia. Es oriunda de Europa y Asia sudoccidental." +
                " Es la especie más cultivada del género Allium",R.drawable.zanahoria)

        verduras.add(verd1)
        verduras.add(verd2)
        verduras.add(verd3)
        verduras.add(verd4)
        verduras.add(verd5)


    }



}