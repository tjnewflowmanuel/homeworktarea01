package com.edilberto.fragmentos.verdura

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import com.edilberto.fragmentos.R
import com.edilberto.fragmentos.model.VerduraEntity

class VerduraActivity : AppCompatActivity() , onVerduraListener {

    private lateinit var verduraFragment: VerduraFragment
    private  lateinit var  detalleVerduraFragment: Detalle_verdura_Fragment
    private lateinit var  fragmenManager: FragmentManager



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verdura)
        fragmenManager=supportFragmentManager
        verduraFragment=fragmenManager.findFragmentById(R.id.fragVerdura) as VerduraFragment
        detalleVerduraFragment =fragmenManager.findFragmentById(R.id.fragDetalle) as Detalle_verdura_Fragment


    }

    override fun selectedItem(verdura: VerduraEntity) {
            detalleVerduraFragment.obtenerVerduras(verdura)
    }

    override fun renderFirst(verdura: VerduraEntity?) {
    verdura?.let {
        selectedItem(it)
    }
    }
}