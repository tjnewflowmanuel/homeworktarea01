package com.edilberto.fragmentos.verdura

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.edilberto.fragmentos.R
import com.edilberto.fragmentos.model.VerduraEntity
import kotlinx.android.synthetic.main.fragment_detalle_verdura_.*

class Detalle_verdura_Fragment : Fragment() {

    private var listener : onVerduraListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detalle_verdura_, container, false)
    }


    fun obtenerVerduras(verdura : VerduraEntity){

        ivDetailImagen.setImageResource(verdura.foto)
        tvDetailDescripcion.text=verdura.descripcion
        tvdetailPrecio.text=verdura.precio
        tvdetailNombre.text=verdura.nombre



    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is onVerduraListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnContactListener")
        }
    }


    override fun onDetach() {
        super.onDetach()
        listener=null
    }




}