package com.edilberto.fragmentos.verdura

import com.edilberto.fragmentos.model.VerduraEntity

interface onVerduraListener {

    fun selectedItem(verdura: VerduraEntity)
    fun renderFirst(verdura: VerduraEntity?)
}